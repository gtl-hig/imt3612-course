#  About #

This repository and Wiki are for NTNU IMT3612 course: GPU Programming.
The course follows the Udacity CS344 course and the default CUDA code
examples. We will try to keep it flexible so that typical usage patterns 
can be tested with OpenCL equivalent code. 


# IMT3612 GPU Programming #
The exam will be live

Time: 09:00 24.05.2017 - 09:00 24.05.2017

Location: Git Repository.  Work where you like.  Oral presentation is 10 mins per person on the 28th/29th of May 2016.

Requirements: Fork the examination Repository https://bitbucket.org/simonmccallum/imt3612_exam. Add your name to then end of the name e.g. imt3612_exam_SimonMcCallum.

Add Simon and the External to have read access to the repository.

You are expected to commit at least once each hour of work.  The exam should be about 8 hours of work, so there should be a minimum of 8 commits.  The commits are important as they will form part of the mark related to development process.  

You will be expected to add material to the Readme.md file.


# Course material

## Setup

[CUDA and NVidia setup materials](https://developer.nvidia.com/cuda-downloads)

Help in setting up your CUDA programming environments and download the most recent drivers for your GPUs.

[AMD setup material](http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/)

Drivers and SDK for AMD GPUs. 

[Android SDK for NVidia CUDA for Tegra K1](https://developer.nvidia.com/tegra-android-development-pack)

Tegra Android development toolkit.





# [Udacity Course](https://www.udacity.com/course/cs344)

This is the main source of lecture materials that this course follows. We will follow this course, with each segment matching the course week in Gjovik. 



# [CUDA Training Zone](http://developer.nvidia.com/cuda-training)

This is the excellent source of additional course materials and additional lectures on various topics related to GPU programming. We will provide more detailed resources as the course progresses, in relation to individual topics that we cover in the lectures. 


# [Vulkan] (https://www.khronos.org/vulkan/)


# [OpenCL Training Zone](developer.amd.com/tools-and-sdks/opencl-zone/opencl-resources/opencl-course-introduction-to-opencl-programming/)

Introduction to OpenCL


# Projects
* [AES OpenCL](https://bitbucket.org/peckto/cl\_aes)